/**
 * @desc SANDBOX -> SHARED BY ALL MODULES -> PROVIDE REQUESTS AND PUB SUB MANAGER - ALSO PROVIDE ACCESS TO APP
 * @param NULL
 * @return NULL
 * @author UX TEAM - @VTEX
 */

// IE8 workaround
if (!window.console) window.console = {};
if (!window.console.log) window.console.log = function () {};

var Sandbox = (function(application){

    //
    // COMPOSITE VIEW MODEL REF
    //
    var app = application;

    //
    // REQUEST DAO OBJ
    //
    // self.dao = new DaoMock(self); // SET TO MOCK IF YOU WANNA USE IT
    self.dao = new DaoApi(self); // SET TO API IF YOU WANNA USE REAL SERVICE
    self.dao.defineAmplifyCalls(); // START AMPLIFY

    return {
        self: this,
        app: app,

        genericDataError: function(jqXHR, textStatus){
            var msg = jqXHR.getResponseHeader("x-vtex-error-message");
            var code = jqXHR.getResponseHeader("x-vtex-error-code");
            var msgDecoded = decodeURIComponent(msg);
            
            app.selectedScreen().UI.Loading(false);
            app.selectedScreen().UI.errorMessage(msgDecoded);
        }
    };
});
