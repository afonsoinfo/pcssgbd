//
// VARS GLOBALS
//

GLOBALS = {};
GLOBALS.baseUrl = "http://" + location.host;

//
// HELPERS
//
ko.myToJSON = function(obj) {
    return JSON.stringify(ko.toJS(obj), function (key, val) {
        return key === '__ko_mapping__' ? undefined : val;
    });
};

//
// KO
//
ko.bindingHandlers.textFade = {
	update: function(element, valueAcessor, allBindings, data, context){
		ko.bindingHandlers.text.update(element, valueAcessor);
	}
};

ko.bindingHandlers.fadeAlert = {
    init: function (element, valueAccessor) {
        var value = valueAccessor();
    },
    update: function (element, valueAccessor) {
        var value = valueAccessor();
        if (ko.utils.unwrapObservable(value)) {
            $(element).animate({
                top: 30
            }, {
                duration: 700,
                specialEasing: {
                    height: 'easeOutBounce'
                }
            });
        } else {
            $(element).animate({
                top: -80
            }, {
                duration: 700,
                specialEasing: {
                    height: 'easeOutBounce'
                }
            });
        }
    }
};

ko.bindingHandlers.fadeVisible = {
    init: function (element, valueAccessor) {
        var value = valueAccessor();
        $(element).toggle(ko.utils.unwrapObservable(value));
    },
    update: function (element, valueAccessor) {
        var value = valueAccessor();
        if (ko.utils.unwrapObservable(value)) {
            $(element).fadeIn();
        } else {
            $(element).hide();
        }
    }
};

ko.bindingHandlers.flash = {
    init: function(element) {
        $(element).hide();
    },
    update: function(element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        if (value) {
            $(element).stop().hide().text(value).fadeIn(function() {
                clearTimeout($(element).data("timeout"));
                $(element).data("timeout", setTimeout(function() {
                    $(element).fadeOut();
                    valueAccessor()(null);
                }, 3000));
            });
        }
    },
    timeout: null
};