/**
 * @desc VIEWMODEL THAT CONTROL OTHERS VIEWMODEL
 * @param NULL
 * @return NULL
 * @author UX TEAM - @VTEX
 */

(function (window, document, undefined) {

    compositeViewModelDefinition = function(_screenId, _hashId) {
        var self = this;

        // SANDBOX
        self.sandbox = new Sandbox(self);

        // VARS
        self.ui = {};
        self.ui.actions = {};

        // HEADER
        self.header = {};
        self.header = new HeaderViewModel();

        // CONTROLE DE TELAS
        self.avaiableScreens = ["builder"];
        self.screens = {};
        self.selectedScreen = ko.observable(null);
        self.selectedScreenId = ko.observable(null);

        // CONTROLE DE ESCRITA DE HASH
        self.setCurrentHash = function() {
            // STOP HASHER LISTENER
            hasher.changed.active = false;

            // SET CURRENT HASH
            var hash = self.selectedScreenId();
            hasher.setHash(hash);

            // START HASHER LISTENER
            hasher.changed.active = true;
        };
        
        // CONTROLE DE TROCA DE PÁGINAS
        self.selectedScreenId.subscribe(function(newValue) {

            // IF NEW ID IS IN AVAIABLE SCREENS
            if ($.inArray(newValue, self.avaiableScreens) != -1)
            {
                // TEST IF SCREEN ISN'T STARTED
                if (self.screens[newValue] == void(0))
                {
                    if (newValue === "builder") {
                        self.screens[newValue] = new BuilderViewModel(self.sandbox);
                    }
                }

                // INICIA O MODULO SELECIONADO
                self.screens[newValue].actions.startModule();
            }
            else //CASO A TELA NÃO EXISTA VAI PARA HOME
            {
                self.selectedScreenId("example-module");
                newValue = "example-module";
                if (self.screens[newValue] === void(0)) {
                    self.screens[newValue] = new BuilderViewModel();
                    self.screens[newValue].actions.startModule();
                }
            }

            self.setCurrentHash();
            self.selectedScreen(self.screens[newValue]);
        });

        // APP INIT
        self.initApp = function(_screenId, _hashId) {
            
            // IF HAS SELECTED SCREEN
            if(_screenId) {
                // CARREGA A ID DE SCREEN A PARTIR DA URL
                self.selectedScreenId(_screenId);
            } else {
                // SE NÃO TEM SCREEN ID VAMOS PARA O CONSTRUTOR
                self.selectedScreenId('builder');
            }
        };
        
        // INIT CALL
        self.initApp(_screenId, _hashId);

        // GENERIC LOADER
        self.ui.loading = ko.computed(function(){
            return self.selectedScreen().ui.loading();
        });

        // VIEW
        self.addHtmlListeners = function(){
            if (self.selectedScreen()){
                self.selectedScreen().view.addHtmlListeners();
            }
        };

        // CUSTOM EVENT HANDLERS
        amplify.subscribe("change-current-page", function(pageId) {
            self.selectedScreenId(pageId);
        });

        amplify.subscribe("change-current-hash", function() {
            self.setCurrentHash();
        });
    };

}(this, document));

//APP INIT
$(document).ready(function(){

    // JUST FOR IE
    if (!window.console) {
        window.console = {
            log:function () {}
        };
    }

    // TPL ENGINE
    infuser.defaults.templateUrl = 'scripts/modules';

    // MASTER VIEWMODEL
    compositeViewModel = null;

    //CAPTURA DE ROTAS PADRÃO
    crossroads.addRoute('/:screenId:/:hashId:', function (screenId, hashId){
        if (!compositeViewModel){
            compositeViewModel = new compositeViewModelDefinition(screenId, hashId);
            ko.applyBindings(compositeViewModel);
        } else {
            compositeViewModel.initApp(screenId, hashId);
        }
    });

    // crossroads.routed.add(console.log, console); //log all routes
    hasher.initialized.add( function(newHash, oldHash)  { crossroads.parse(newHash); });
    hasher.changed.add( function(newHash, oldHash)  { crossroads.parse(newHash); });
    hasher.init();
});