/**
 * @desc DAO -> MANAGE DATA ACCESS -> API
 * @param NULL
 * @return NULL
 * @author UX TEAM - @VTEX
 */

var DaoApi = (function(application){

    //
    // PRIVATE VARS
    //
    var app = application;
    var privateVars = {};

    return {

        //
        // PUBLIC FUNCTIONS
        //

        defineAmplifyCalls: function(){

            //
            // DEFINE OUR DECODER
            //
            amplify.request.decoders.genericDecoder = function(response, status, xhr, success, error){
                if (!xhr.getResponseHeader("x-vtex-error-code")){
                    success(response);
                } else {
                    console.log(xhr);
                    error(xhr, status);
                }
            };
        }
    };
});
