/**
 * @desc DAO -> MANAGE DATA ACCESS -> MOCK
 * @param NULL
 * @return NULL
 * @author UX TEAM - @VTEX
 */

var DaoMock = (function(application){

    //
    // PRIVATE VARS
    //
    var app = application;
    var privateVars = {};

    return {

        //
        // PUBLIC VARS
        //
        mocksPath : '/scripts/services/mocks',
        app : application,


        //
        // PUBLIC FUNCTIONS
        //

        defineAmplifyCalls: function(){
            
           
        }
    };
});