/**
* @desc VIEWMODEL OF A MODULE
* @param NULL
* @return NULL
* @author UX TEAM - @VTEX
*/

(function (window, document, undefined){

    BuilderViewModel = function(sbox){
        
        var self = this;
        self.sandbox = sbox;

        self.actions = {};
        self.view = {};
        self.ui = {};
        self.ui.loading = ko.observable(false);
        self.template = "builder/html/builder";
        self.ui.actions = {};
        self.ui.errorMessage = ko.observable("");
        self.ui.successMessage = ko.observable("");

        // SLOTS DISPONIVEIS PARA A CONSTRUÇÃO
        self.slots = ko.observableArray([]);

        // MÓDULOS DISPONÍVEIS
        self.availableModules = ko.observableArray([]);
        self.availableModules.id = "availableModules";

        // MODULO SELECIONADO PARA EDICAO
        self.selectedModule = ko.observable(null);

        // CORES DISPONIVEIS
        self.availableColors = ko.observableArray(["branco", "madeira", "verniz"]);
        
        // MSGS DE SUCESSO E ERRO
        self.lastAction = ko.observable(null);
        self.lastError = ko.observable(null);
        
        // ALTURA MAXIMA DE UM CONJUNTO DE MODULOS
        self.maximumModules = 2;

        // CONTROLE PARA EXIBICAO DO BT DE SALVAR
        self.wasChanged = ko.observable(false);
		


        //
        // UI ACTIONS
        //
        
        //
        // FUNCTION TO CHANGE PAGE
        //
        self.ui.actions.changePage = function(data, evt){
            var pageName = $(evt.currentTarget).data('name');
            amplify.publish("change-current-page", pageName);
        };

        self.ui.actions.openConfigModal = function(data, evt){
            self.selectedModule(data);
            $(".config-modal").modal("show");
        };

        self.ui.actions.closeConfigModal = function(){
            $("config-modal").modal("hide");
        };

        // VIEW ACTIONS
        self.view.addHtmlListeners = function(){
            self.view.removeHtmlListeners();
        };

        self.view.removeHtmlListeners = function(){
            
        };


        // CLASS MODULE
        var Module = function(id, description, width, height, color) {
            this.id =  ko.observable(id);
            this.width =  ko.observable(width);
            this.height =  ko.observable(height);
            this.description = ko.observable(description);
            this.color =  ko.observable(color);
            this.drawer =  ko.observable(false);
            this.door =  ko.observable(false);
            this.shelf =  ko.observable(false);
            this.value =  ko.observable(undefined);
            this.position =  ko.observable(undefined);
        };


        var Table = function(id, modules, width) {
            this.modules = ko.observableArray(modules);
            this.modules.id = id;
            this.width = width;
        };

        var SeatingChartModel = function(slots, availableModules) {
            self.slots(slots);
            self.availableModules(availableModules);
            self.availableModules.valueHasMutated();

             for (var i = 0; i < self.slots().length; i++) {
                self.slots()[i].modules([]);
            };
        };

        self.isTableFull = function(parent) {
            return parent().length < self.maximumModules;
        };
		
		function addColumn(){
			
			var numberOfColumns = document.getElementById("main").children.length;
			var lastColumn = document.getElementById("main").children[numberOfColumns - 1];
			var lastColumnNumber = lastColumn.children[0].innerHTML;
			var newNumber = parseInt(lastColumnNumber) + 1; 
			
			
			self.tables.push(new Table(""+newNumber+"", [], "30px"));
		}
		
		function cloneModules(moduleClone){
			newId = Math.floor((Math.random()*100)+moduleClone.id);
			newName = ""+moduleClone.textName[0]+""+moduleClone.textName[1]+""
			self.availableModules.push(new Module(newId, newName, moduleClone.width, moduleClone.height));
			self.availableModules.valueHasMutated();
			
		}
		

        self.updateLastAction = function(arg) {
			
            var found,
                parent = arg.targetParent;

            if (parent.id !== "availableModules") {
                self.lastAction(arg.item.description() + " incluido na " + arg.targetParent.id + "a coluna");
                self.wasChanged(true);
            }
			
			if (arg.sourceParent.id === "availableModules"){
				var moduleClone = {
					id: arg.item.id,
					textName: arg.sourceParentNode.textContent,
					width: arg.item.width,
					height: arg.item.height
				};
				cloneModules(moduleClone);
			}
        };

        //verify that if a fourth member is added, there is at least one member of each gender
        self.verifyAssignments = function(arg) {
		
		
            var found,
                parent = arg.targetParent;

            if (parent.id !== "availableModules" && parent().length === 1 && parent.indexOf(arg.item) < 0) {
                height = arg.item.height;
                width = arg.item.width;
                if (!ko.utils.arrayFirst(parent(), function(module) { return module.height !== "121px";})) {
                    self.lastError("Uma coluna nao pode ter mais de 2.4 metros de altura");
					addColumn();
                    arg.cancelDrop = true;
                }
                else if (!ko.utils.arrayFirst(parent(), function(module) { return height !== "121px";})) {
                    self.lastError("Uma coluna nao pode ter mais de 2.4 metros de altura");
					addColumn();
                    arg.cancelDrop = true;
                }
                else if (!ko.utils.arrayFirst(parent(), function(module) { return module.width === width;})) {
                    self.lastError("Uma coluna so pode conter modulos de mesma largura");
					addColumn();
                    arg.cancelDrop = true;
                }
            }
        };

        var seats = [
        /*    new Table("M",  [
                new Module(1, "g 0.6x1.2", "30px", "60px"),
                new Module(2, "p 1.2x1.2", "60px", "60px"),
                new Module(3, "p 1.2x2.4", "60px", "121px"),
                new Module(4, "p 0.6x2.4", "30px", "121px"),
                new Module(5, "g 0.6x1.2", "30px", "60px")
            ], "60px"),
        */  new Table("1", [], "30px"),
            new Table("2", [], "30px"),
            new Table("3", [], "30px"),
            new Table("4", [], "30px")
        ];

         //
        // CORE ACTIONS
        //
		
		
        self.actions.startModule = function() {

            // build initial modules
            var initialModules = [
                new Module(1, "g1", "30px", "60px", "branco"),
                new Module(2, "g2", "60px", "60px", "branco"),
                new Module(3, "g3", "30px", "121px", "madeira"),
                new Module(4, "g4", "60px", "121px", "branco"),
                new Module(5, "p1", "30px", "60px", "verniz"),
                new Module(6, "p2", "60px", "60px", "branco"),
                new Module(7, "p3", "30px", "121px", "branco"),
                new Module(8, "p4", "60px", "121px", "verniz")
            ];

            // ESVAZIA AS 
            self.slots([]);
            self.availableModules([]);
            SeatingChartModel(seats, initialModules);
			

            ko.bindingHandlers.sortable.beforeMove = self.verifyAssignments;
            ko.bindingHandlers.sortable.afterMove = self.updateLastAction;
			
			

            //self.ui.loading(true);

            //
            // CLEAR MSGS
            //
            self.ui.errorMessage("");
            self.ui.successMessage("");
			
			
        };
    };

    BuilderViewModel.id = "builder";

}(this, document));
