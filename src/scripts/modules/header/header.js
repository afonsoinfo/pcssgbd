/**
 * @desc VIEWMODEL OF A MODULE
 * @param NULL
 * @return NULL
 * @author UX TEAM - @VTEX
 */

(function (window, document, undefined){

    HeaderViewModel = function(sbox){

        var self = this;
        self.sandbox = sbox;

        self.actions = {};
        self.view = {};
        self.ui = {};
        self.ui.loading = ko.observable(false);
        self.template = "header/html/header";
        self.ui.actions = {};
        self.ui.errorMessage = ko.observable("");
        self.ui.successMessage = ko.observable("");

        //
        // UI ACTIONS
        //

        // FUNCTION TO CHANGE PAGE
        self.ui.actions.changePage = function(data, evt){
            var pageName = $(evt.currentTarget).data('name');
            amplify.publish("change-current-page", pageName);
        };

        //
        // CORE ACTIONS
        //
        self.actions.startModule = function() {
            //self.ui.loading(true);

            //
            // CLEAR MSGS
            //
            self.ui.errorMessage("");
            self.ui.successMessage("");
        };

        // VIEW ACTIONS
        self.view.addHtmlListeners = function(){
            self.view.removeHtmlListeners();
        };

        self.view.removeHtmlListeners = function(){

        };
    };

    HeaderViewModel.id = "header";

}(this, document));
